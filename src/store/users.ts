import { Module, VuexModule, MutationAction, getModule } from 'vuex-module-decorators';
import store from '@/store/index';
import { User } from '@/classes/users/users';

@Module({
  name: 'modules/userModule',
  dynamic: true,
  store, // allows me to add to store in each file instead of index
})
export default class Users extends VuexModule {
  user: User = new User();

  @MutationAction({ mutate: ['user'] })
  public async SetUser(user: User) {
    return {
      user,
    };
  }

  get GetUser(): User {
    return this.user;
  }
}

export const UsersModule = getModule(Users);
