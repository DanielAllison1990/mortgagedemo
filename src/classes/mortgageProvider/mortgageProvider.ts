import { MortgageType } from '../enums/MortgageType';

export class MortgageProvider {
  public Id = 0;
  public Lender = '';
  public InterestRate = 0;
  public Type = MortgageType.Variable;
  public LoanToValue = 0;

  constructor(provider: MortgageProvider | null | undefined = null) {
    if (provider) {
      this.Id = provider.Id;
      this.Lender = provider.Lender;
      this.InterestRate = provider.InterestRate;
      this.Type = provider.Type;
      this.LoanToValue = provider.LoanToValue;
    }
  }
}
