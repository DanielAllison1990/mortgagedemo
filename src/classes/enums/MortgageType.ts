export enum MortgageType {
    'Variable' = 1,
    'Fixed' = 2
}
