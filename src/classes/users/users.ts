export class User {
  public Id = 0;
  public FirstName = '';
  public LastName = '';
  public Email = '';
  public DateOfBirth = null;
  public get FullName(): string {
    return `${this.FirstName} ${this.LastName}`;
  }

  constructor(user: User | null | undefined = null) {
    if (user) {
      this.Id = user.Id;
      this.FirstName = user.FirstName;
      this.LastName = user.LastName;
      this.Email = user.Email;
      this.DateOfBirth = user.DateOfBirth;
    }
  }
}
