import { User } from '@/classes/users/users';
import axios, { AxiosResponse } from 'axios';

export class UserService {
  url = 'https://localhost:5001/api';
  CreateUser(user: User): Promise<number> {
    return new Promise((resolve, reject) => {
      axios.post(`${this.url}/users`, user).then(
        (response: AxiosResponse<number>) => {
          resolve(response.data);
        },
        (err) => {
          console.log(err);
          reject(err);
        },
      );
    });
  }
}
