import { MortgageProvider } from '@/classes/mortgageProvider/mortgageProvider';
import { MortgageSearch } from '@/classes/mortgageProvider/mortgageSearch';
import axios, { AxiosResponse } from 'axios';

export class MortgageProviderService {
  url = 'https://localhost:5001/api';
  GetMortgageProviders(mortgageForm: MortgageSearch): Promise<MortgageProvider[]> {
    return new Promise((resolve, reject) => {
      axios.get(`${this.url}/mortgageprovider/GetProviderBasedOnLTV/?propertyValue=${mortgageForm.PropertyValue}&depositAmount=${mortgageForm.DepositAmount}&userId=${mortgageForm.UserId}`).then(
        (response: AxiosResponse<MortgageProvider[]>) => {
          console.log(response.data);
          resolve(response.data);
        },
        (err) => {
          console.log(err);
          reject(err);
        },
      );
    });
  }
}
